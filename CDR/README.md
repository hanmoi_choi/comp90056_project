### How to Run
 mvn -f pom.xml compile exec:java -Dexec.classpathScope=compile -Dexec.mainClass=edu.unimelb.streaming.CdrMarketingTemplate
### Add resource files
Add cdr text files under ../comp90056_project/CDR/src/main/resources
Change names as cdr_rawsample1.txt and cdr_rawsample2.txt