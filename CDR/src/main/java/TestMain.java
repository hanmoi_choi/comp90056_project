import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.util.EmailUtil;
import edu.unimelb.streaming.view.CdrMainGUI;

public class TestMain {

  public static void main(String[] args) {
//    EmailUtil emailUtil = new EmailUtil();
//    emailUtil.sendEmail();
    CdrTemplateController controller = CdrTemplateController.getInstance();
    CdrMainGUI window = new CdrMainGUI(controller);
  }
}
