package edu.unimelb.streaming.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA. User: streamsadmin Date: 9/17/13 Time: 12:18 AM To change this
 * template use File | Settings | File Templates.
 */
public class StopWorldRemoval {

  public static HashSet<String> createStopWordSet() {
    HashSet<String> stopSet = new HashSet<String>();
    try {
      File f = new File("stopwords.txt");

      BufferedReader br;
      br = new BufferedReader(new FileReader(f));
      String line;
      while ((line = br.readLine()) != null) {
        // process the line.
        // System.out.println("line is " + line);
        stopSet.add(line);
      }
      br.close();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      System.out.println("ERROR OCCURRED WHEN READING");
    }

    return stopSet;

  }

}
