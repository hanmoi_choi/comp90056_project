package edu.unimelb.streaming.view;

import java.awt.*;
import java.util.*;
import java.util.List;

import javax.swing.*;

public class CanvasPanel extends JPanel {

  List<Integer> callQualities = new LinkedList<Integer>();

  public CanvasPanel() {
    callQualities = new LinkedList<Integer>();
  }

  public void addCallQualityIndex(Float callQuality){
    Integer yPosition = Math.round(650 - (650 * callQuality)) + 50;
    if(callQualities.size() > 100){
      callQualities = callQualities.subList(1, 100);
      callQualities.add(100, yPosition);
    }
    else {
      callQualities.add(yPosition);
    }
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g.create();

    g2.setColor(Color.black);
    for(int i = 50, j = 100; i < 650 ; i = i+ 12, j--){
      g2.drawLine(52, i, 58, i);
      if(j % 10 == 0){
        g2.drawString(String.valueOf(j), 25, i);
      }
    }
    g2.setStroke(new BasicStroke(3,
                                 BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
    g2.drawLine( 55, 10, 55, 650);
    g2.drawLine( 55, 650, 875, 650);

    g2.setColor(Color.RED);
    g2.setStroke(new BasicStroke(2,
                                 BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));

    g2.drawLine( 25, 110, 850, 110);

    g2.setColor(Color.BLUE);
    g2.setStroke(new BasicStroke(1,
                                 BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));

    Integer[] yPositions = callQualities.toArray(new Integer[0]);
    for(int i = 0, j = 55 ; i < yPositions.length - 1; i ++, j += 8){
      g2.drawLine(j, yPositions[i], j + 8, yPositions[i + 1]);
    }
  }
}
