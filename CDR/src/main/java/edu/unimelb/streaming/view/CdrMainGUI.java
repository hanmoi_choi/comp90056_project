package edu.unimelb.streaming.view;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.awt.EventQueue;

import javax.swing.*;

import java.awt.BorderLayout;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Color;

import java.awt.Font;

import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observable;
import edu.unimelb.streaming.controller.Observer;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static edu.unimelb.streaming.controller.Event.TupleCount;

import javax.swing.border.TitledBorder;

import java.awt.GridLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class CdrMainGUI implements Observer {

  private JFrame frame;
  private JTextField tfCommonSpaceSize;
  private JTextField tfMCTDurationValue;
  private JTextField tfMCTRewordValue;
  private final ButtonGroup btnGpCdrSrc = new ButtonGroup();
  private JLabel lblTotalCdrsValue;
  private Observable observable;
  private JLabel lblTotalCallDropsValue;
  private Long totalTupleCount;
  private CallDropTable callDropTable;
  private MostNatCallerTable mostNatCallerTable;
  private MostIntCallerTable mostInternationalCallerTable;
  private MostLocalCallerTable mostLocalCallerTable;
  private JLabel lblProceesRate;

  private ExecutorService executor;

  private DateTime startTime;
  private JLabel elapsedTime;
  private MostCallerTable mostCallerTable;
  private JTextField tfCDTCallDropValue;
  private JTextField tfCDTWindowSizeValue;
  private JTextField tfMCTWindowSizeValue;
  private JTextField tfMICTDurationValue;
  private JTextField tfMICTRewordValue;
  private JTextField tfMICTWindowSizeValue;
  private JTextField tfMLCTDurationValue;
  private JTextField tfMLCTRewordValue;
  private JTextField tfMLCTWindowSizeValue;
  private JTextField tfMNCTDurationValue;
  private JTextField tfMNCTRewordValue;
  private JTextField tfMNCTWindowSize;
  private JButton btnMostNatCallerSetNewRule;
  private JTextField tfUserToDisplayValue;
  private CanvasPanel canvasPanel;
  private static boolean isStarted;
  private Long expired;
  private JTextField tfMCTWorkerValue;
  private JTextField tfMITWorkerValue;
  private JTextField tfMLTWorkerValue;
  private JTextField tfMNTWorkerValue;
  private JTextField tfCDTWorkerValue;
  private JTextField tfWorkerProcesses;
  private JTextField tfDeduplicateWindowSize;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          CdrMainGUI window = new CdrMainGUI();
          window.frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public CdrMainGUI() {
    initialize();
    frame.setVisible(true);

  }

  /**
   * Create the application.
   */
  public CdrMainGUI(Observable observable) {
    this.observable = observable;
    observable.register(TupleCount, this);
    observable.register(Event.TotalCallDrop, this);
    observable.register(Event.CallDropList, this);
    observable.register(Event.MostCallerList, this);
    observable.register(Event.MostIntCallerList, this);
    observable.register(Event.MostNatCallerList, this);
    observable.register(Event.MostLocalCallerList, this);
    initialize();
    frame.setVisible(true);
    startTime = DateTime.now(DateTimeZone.UTC);
    executor = Executors.newFixedThreadPool(1);
    totalTupleCount = 0L;
    expired = 1L;
    isStarted = false;
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    frame = new JFrame();
    frame.setBounds(100, 100, 949, 791);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    tabbedPane.setToolTipText("Cdr Template Configuration");
    frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

    JPanel pnConfiguration = new JPanel();
    pnConfiguration.setBackground(Color.DARK_GRAY);
    tabbedPane.addTab("Config", null, pnConfiguration, null);
    pnConfiguration.setLayout(new BorderLayout(0, 0));

    JPanel emailPanel = new JPanel();
    emailPanel.setBackground(Color.DARK_GRAY);
    emailPanel.setPreferredSize(new Dimension(10, 35));
    pnConfiguration.add(emailPanel, BorderLayout.NORTH);
    emailPanel.setLayout(null);

    JPanel templateSettingPanel = new JPanel();
    pnConfiguration.add(templateSettingPanel, BorderLayout.CENTER);
    GridBagLayout gbl_templateSettingPanel = new GridBagLayout();
    gbl_templateSettingPanel.columnWidths = new int[]{0, 0, 2};
    gbl_templateSettingPanel.rowHeights = new int[]{0, 0, 0, 3};
    gbl_templateSettingPanel.columnWeights = new double[]{1.0, 1.0,
                                                          Double.MIN_VALUE};
    gbl_templateSettingPanel.rowWeights = new double[]{1.0, 1.0, 1.0,
                                                       Double.MIN_VALUE};
    templateSettingPanel.setLayout(gbl_templateSettingPanel);

    JPanel pnCommonSetting = new JPanel();
    pnCommonSetting.setBackground(Color.DARK_GRAY);
    pnCommonSetting.setLayout(null);
    GridBagConstraints gbc_pnCommonSetting = new GridBagConstraints();
    gbc_pnCommonSetting.insets = new Insets(0, 0, 5, 5);
    gbc_pnCommonSetting.fill = GridBagConstraints.BOTH;
    gbc_pnCommonSetting.gridx = 0;
    gbc_pnCommonSetting.gridy = 0;
    templateSettingPanel.add(pnCommonSetting, gbc_pnCommonSetting);

    JLabel lblSpaceSize = new JLabel("Space Size");
    lblSpaceSize.setForeground(Color.WHITE);
    lblSpaceSize.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblSpaceSize.setBounds(6, 41, 115, 23);
    pnCommonSetting.add(lblSpaceSize);

    tfCommonSpaceSize = new JTextField();
    tfCommonSpaceSize.setText("1000");
    tfCommonSpaceSize.setColumns(10);
    tfCommonSpaceSize.setBounds(133, 38, 134, 28);
    pnCommonSetting.add(tfCommonSpaceSize);

    JLabel lblCallDropTemplate = new JLabel("Common Setting");
    lblCallDropTemplate.setForeground(Color.WHITE);
    lblCallDropTemplate.setFont(new Font("Georgia", Font.PLAIN, 16));
    lblCallDropTemplate.setBounds(6, 6, 173, 23);
    pnCommonSetting.add(lblCallDropTemplate);

    JButton btnCommonSetNewRule = new JButton("Set New Rule");
    btnCommonSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String value = tfUserToDisplayValue.getText();
        Long valueAsLong = Long.parseLong(value);
        observable.notify(Event.HowManyToDisplay, valueAsLong);
      }
    });

    btnCommonSetNewRule.setBounds(337, 5, 117, 29);
    pnCommonSetting.add(btnCommonSetNewRule);

    JLabel lblUserToDisplay = new JLabel("User To Display");
    lblUserToDisplay.setForeground(Color.WHITE);
    lblUserToDisplay.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblUserToDisplay.setBounds(6, 89, 115, 23);
    pnCommonSetting.add(lblUserToDisplay);

    tfUserToDisplayValue = new JTextField();
    tfUserToDisplayValue.setText("50");
    tfUserToDisplayValue.setColumns(10);
    tfUserToDisplayValue.setBounds(133, 86, 134, 28);
    pnCommonSetting.add(tfUserToDisplayValue);

    JLabel lblExecutorWorker = new JLabel("Worker Processes");
    lblExecutorWorker.setForeground(Color.WHITE);
    lblExecutorWorker.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblExecutorWorker.setBounds(6, 127, 115, 23);
    pnCommonSetting.add(lblExecutorWorker);

    JLabel lblProcesses = new JLabel("Processes");
    lblProcesses.setForeground(Color.WHITE);
    lblProcesses.setBounds(267, 130, 73, 16);
    pnCommonSetting.add(lblProcesses);

    tfWorkerProcesses = new JTextField();
    tfWorkerProcesses.setText("5");
    tfWorkerProcesses.setColumns(10);
    tfWorkerProcesses.setBounds(133, 126, 134, 28);
    pnCommonSetting.add(tfWorkerProcesses);

    JLabel lblRows = new JLabel("Rows");
    lblRows.setForeground(Color.WHITE);
    lblRows.setBounds(267, 92, 73, 16);
    pnCommonSetting.add(lblRows);

    JLabel lblK = new JLabel("K");
    lblK.setForeground(Color.WHITE);
    lblK.setBounds(267, 44, 73, 16);
    pnCommonSetting.add(lblK);

    JLabel lblDeduplicationWindowSize = new JLabel("Deduplication Window Size");
    lblDeduplicationWindowSize.setForeground(Color.WHITE);
    lblDeduplicationWindowSize.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblDeduplicationWindowSize.setBounds(6, 163, 173, 23);
    pnCommonSetting.add(lblDeduplicationWindowSize);

    JLabel lblSeconds = new JLabel("Seconds");
    lblSeconds.setForeground(Color.WHITE);
    lblSeconds.setBounds(337, 166, 73, 16);
    pnCommonSetting.add(lblSeconds);

    tfDeduplicateWindowSize = new JTextField();
    tfDeduplicateWindowSize.setText("60");
    tfDeduplicateWindowSize.setColumns(10);
    tfDeduplicateWindowSize.setBounds(191, 160, 134, 28);
    pnCommonSetting.add(tfDeduplicateWindowSize);

    JPanel pnCallDropTmp = new JPanel();
    pnCallDropTmp.setLayout(null);
    pnCallDropTmp.setBackground(Color.DARK_GRAY);
    GridBagConstraints gbc_pnCallDropTmp = new GridBagConstraints();
    gbc_pnCallDropTmp.insets = new Insets(0, 0, 5, 0);
    gbc_pnCallDropTmp.fill = GridBagConstraints.BOTH;
    gbc_pnCallDropTmp.gridx = 1;
    gbc_pnCallDropTmp.gridy = 0;
    templateSettingPanel.add(pnCallDropTmp, gbc_pnCallDropTmp);

    JCheckBox cbCallDropEnabled = new JCheckBox("Enabled");
    cbCallDropEnabled.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Boolean isEnabled = e.getStateChange() == ItemEvent.SELECTED;
        observable.notify(Event.DroppedCallEnable, isEnabled);
      }
    });
    cbCallDropEnabled.setSelected(true);
    cbCallDropEnabled.setForeground(Color.WHITE);
    cbCallDropEnabled.setFont(new Font("Georgia", Font.PLAIN, 14));
    cbCallDropEnabled.setBounds(365, 6, 90, 23);
    pnCallDropTmp.add(cbCallDropEnabled);

    JLabel label = new JLabel("Call Drop");
    label.setForeground(Color.WHITE);
    label.setFont(new Font("Georgia", Font.PLAIN, 14));
    label.setBounds(6, 52, 80, 23);
    pnCallDropTmp.add(label);

    tfCDTCallDropValue = new JTextField();
    tfCDTCallDropValue.setText("5");
    tfCDTCallDropValue.setColumns(10);
    tfCDTCallDropValue.setBounds(101, 49, 134, 28);
    pnCallDropTmp.add(tfCDTCallDropValue);

    tfCDTWindowSizeValue = new JTextField();
    tfCDTWindowSizeValue.setText("2");
    tfCDTWindowSizeValue.setColumns(10);
    tfCDTWindowSizeValue.setBounds(101, 96, 134, 28);
    pnCallDropTmp.add(tfCDTWindowSizeValue);

    JLabel label_5 = new JLabel("Window Size");
    label_5.setForeground(Color.WHITE);
    label_5.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_5.setBounds(6, 99, 115, 23);
    pnCallDropTmp.add(label_5);

    JLabel label_6 = new JLabel("Call Drop Template");
    label_6.setForeground(Color.WHITE);
    label_6.setFont(new Font("Georgia", Font.PLAIN, 16));
    label_6.setBounds(6, 6, 173, 23);
    pnCallDropTmp.add(label_6);

    JButton btnCallDropSetNewRule = new JButton("Set New Rule");
    btnCallDropSetNewRule.setBounds(249, 5, 117, 29);
    btnCallDropSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String callDraop = tfCDTCallDropValue.getText();
        Long callDropAsLong = Long.parseLong(callDraop);
        observable.notify(Event.SetCallDropThreshhold, callDropAsLong);

        String windowSize = tfCDTWindowSizeValue.getText();
        Long windowSizeAsLong = Long.parseLong(windowSize);
        observable.notify(Event.CallDropWindowSize, windowSizeAsLong);
      }
    });
    pnCallDropTmp.add(btnCallDropSetNewRule);

    JLabel label_7 = new JLabel("Times");
    label_7.setForeground(Color.WHITE);
    label_7.setBounds(234, 55, 51, 16);
    pnCallDropTmp.add(label_7);

    JLabel label_8 = new JLabel("Seconds");
    label_8.setForeground(Color.WHITE);
    label_8.setBounds(234, 102, 73, 16);
    pnCallDropTmp.add(label_8);

    JLabel label_30 = new JLabel("Worker");
    label_30.setForeground(Color.WHITE);
    label_30.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_30.setBounds(6, 139, 95, 23);
    pnCallDropTmp.add(label_30);

    tfCDTWorkerValue = new JTextField();
    tfCDTWorkerValue.setText("10");
    tfCDTWorkerValue.setColumns(10);
    tfCDTWorkerValue.setBounds(101, 136, 134, 28);
    pnCallDropTmp.add(tfCDTWorkerValue);

    JLabel label_31 = new JLabel("Threads");
    label_31.setForeground(Color.WHITE);
    label_31.setBounds(234, 142, 73, 16);
    pnCallDropTmp.add(label_31);

    JPanel pnMostCallerTmp = new JPanel();
    pnMostCallerTmp.setBackground(Color.DARK_GRAY);
    pnMostCallerTmp.setLayout(null);
    GridBagConstraints gbc_pnMostCallerTmp = new GridBagConstraints();
    gbc_pnMostCallerTmp.insets = new Insets(0, 0, 5, 5);
    gbc_pnMostCallerTmp.fill = GridBagConstraints.BOTH;
    gbc_pnMostCallerTmp.gridx = 0;
    gbc_pnMostCallerTmp.gridy = 1;
    templateSettingPanel.add(pnMostCallerTmp, gbc_pnMostCallerTmp);

    JCheckBox cbMostCallerEnabled = new JCheckBox("Enabled");
    cbMostCallerEnabled.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Boolean isEnabled = e.getStateChange() == ItemEvent.SELECTED;
        observable.notify(Event.MostCallerEnable, isEnabled);
      }
    });
    cbMostCallerEnabled.setSelected(true);
    cbMostCallerEnabled.setForeground(Color.WHITE);
    cbMostCallerEnabled.setFont(new Font("Georgia", Font.PLAIN, 14));
    cbMostCallerEnabled.setBounds(365, 6, 90, 23);
    pnMostCallerTmp.add(cbMostCallerEnabled);

    tfMCTDurationValue = new JTextField();
    tfMCTDurationValue.setText("7");
    tfMCTDurationValue.setColumns(10);
    tfMCTDurationValue.setBounds(101, 64, 134, 28);
    pnMostCallerTmp.add(tfMCTDurationValue);

    tfMCTRewordValue = new JTextField();
    tfMCTRewordValue.setText("1");
    tfMCTRewordValue.setColumns(10);
    tfMCTRewordValue.setBounds(101, 104, 134, 28);
    pnMostCallerTmp.add(tfMCTRewordValue);

    JLabel lblMCTDuration = new JLabel("Duration");
    lblMCTDuration.setForeground(Color.WHITE);
    lblMCTDuration.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblMCTDuration.setBounds(6, 67, 80, 23);
    pnMostCallerTmp.add(lblMCTDuration);

    JLabel lblMCTReword = new JLabel("Reword");
    lblMCTReword.setForeground(Color.WHITE);
    lblMCTReword.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblMCTReword.setBounds(6, 102, 80, 23);
    pnMostCallerTmp.add(lblMCTReword);

    JLabel lblMostCallerTemplate = new JLabel("Most Caller Template");
    lblMostCallerTemplate.setForeground(Color.WHITE);
    lblMostCallerTemplate.setFont(new Font("Georgia", Font.PLAIN, 16));
    lblMostCallerTemplate.setBounds(6, 6, 164, 23);
    pnMostCallerTmp.add(lblMostCallerTemplate);

    JButton btnMostCallerSetNewRule = new JButton("Set New Rule");
    btnMostCallerSetNewRule.setBounds(247, 5, 117, 29);
    btnMostCallerSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String duration = tfMCTDurationValue.getText();
        Long durationAsLong = Long.parseLong(duration);
        observable.notify(Event.MctCallDuration, durationAsLong);

        String reward = tfMCTRewordValue.getText();
        Long rewordAsLong = Long.parseLong(reward);
        observable.notify(Event.MctReward, rewordAsLong);

        String windowSize = tfMCTWindowSizeValue.getText();
        Long windowSizeAsLong = Long.parseLong(windowSize);
        observable.notify(Event.MctWindowSize, windowSizeAsLong);
      }
    });
    pnMostCallerTmp.add(btnMostCallerSetNewRule);

    JLabel lblMCTWindowSize = new JLabel("Window Size");
    lblMCTWindowSize.setForeground(Color.WHITE);
    lblMCTWindowSize.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblMCTWindowSize.setBounds(6, 150, 95, 23);
    pnMostCallerTmp.add(lblMCTWindowSize);

    tfMCTWindowSizeValue = new JTextField();
    tfMCTWindowSizeValue.setText("2");
    tfMCTWindowSizeValue.setColumns(10);
    tfMCTWindowSizeValue.setBounds(101, 144, 134, 28);
    pnMostCallerTmp.add(tfMCTWindowSizeValue);

    JLabel label_3 = new JLabel("Seconds");
    label_3.setForeground(Color.WHITE);
    label_3.setBounds(234, 153, 73, 16);
    pnMostCallerTmp.add(label_3);

    JLabel lblHours = new JLabel("Hours");
    lblHours.setForeground(Color.WHITE);
    lblHours.setBounds(234, 110, 73, 16);
    pnMostCallerTmp.add(lblHours);

    JLabel label_2 = new JLabel("Hours");
    label_2.setForeground(Color.WHITE);
    label_2.setBounds(234, 70, 73, 16);
    pnMostCallerTmp.add(label_2);

    JLabel lblWorker = new JLabel("Worker");
    lblWorker.setForeground(Color.WHITE);
    lblWorker.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblWorker.setBounds(6, 185, 95, 23);
    pnMostCallerTmp.add(lblWorker);

    tfMCTWorkerValue = new JTextField();
    tfMCTWorkerValue.setText("10");
    tfMCTWorkerValue.setColumns(10);
    tfMCTWorkerValue.setBounds(101, 182, 134, 28);
    pnMostCallerTmp.add(tfMCTWorkerValue);

    JLabel lblThreads = new JLabel("Threads");
    lblThreads.setForeground(Color.WHITE);
    lblThreads.setBounds(234, 188, 73, 16);
    pnMostCallerTmp.add(lblThreads);

    JPanel pnMostIntCallerTmp = new JPanel();
    pnMostIntCallerTmp.setLayout(null);
    pnMostIntCallerTmp.setBackground(Color.DARK_GRAY);
    GridBagConstraints gbc_pnMostIntCallerTmp = new GridBagConstraints();
    gbc_pnMostIntCallerTmp.insets = new Insets(0, 0, 5, 0);
    gbc_pnMostIntCallerTmp.fill = GridBagConstraints.BOTH;
    gbc_pnMostIntCallerTmp.gridx = 1;
    gbc_pnMostIntCallerTmp.gridy = 1;
    templateSettingPanel.add(pnMostIntCallerTmp, gbc_pnMostIntCallerTmp);

    JCheckBox cbMostIntCallerEnable = new JCheckBox("Enabled");
    cbMostIntCallerEnable.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Boolean isEnabled = e.getStateChange() == ItemEvent.SELECTED;
        observable.notify(Event.MostIntCallerEnable, isEnabled);
      }
    });
    cbMostIntCallerEnable.setSelected(true);
    cbMostIntCallerEnable.setForeground(Color.WHITE);
    cbMostIntCallerEnable.setFont(new Font("Georgia", Font.PLAIN, 14));
    cbMostIntCallerEnable.setBounds(365, 6, 90, 23);
    pnMostIntCallerTmp.add(cbMostIntCallerEnable);

    tfMICTDurationValue = new JTextField();
    tfMICTDurationValue.setText("4");
    tfMICTDurationValue.setColumns(10);
    tfMICTDurationValue.setBounds(101, 64, 134, 28);
    pnMostIntCallerTmp.add(tfMICTDurationValue);

    tfMICTRewordValue = new JTextField();
    tfMICTRewordValue.setText("1");
    tfMICTRewordValue.setColumns(10);
    tfMICTRewordValue.setBounds(101, 104, 134, 28);
    pnMostIntCallerTmp.add(tfMICTRewordValue);

    JLabel label_4 = new JLabel("Duration");
    label_4.setForeground(Color.WHITE);
    label_4.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_4.setBounds(6, 67, 80, 23);
    pnMostIntCallerTmp.add(label_4);

    JLabel label_9 = new JLabel("Reword");
    label_9.setForeground(Color.WHITE);
    label_9.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_9.setBounds(6, 102, 80, 23);
    pnMostIntCallerTmp.add(label_9);

    JLabel lblMostIntCaller = new JLabel("Most Int Caller Template");
    lblMostIntCaller.setForeground(Color.WHITE);
    lblMostIntCaller.setFont(new Font("Georgia", Font.PLAIN, 16));
    lblMostIntCaller.setBounds(6, 6, 212, 23);
    pnMostIntCallerTmp.add(lblMostIntCaller);

    JButton btnMostIntCallerSetNewRule = new JButton("Set New Rule");
    btnMostIntCallerSetNewRule.setBounds(247, 5, 117, 29);
    btnMostIntCallerSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String duration = tfMICTDurationValue.getText();
        Long durationAsLong = Long.parseLong(duration);
        observable.notify(Event.MitDuration, durationAsLong);

        String reward = tfMICTRewordValue.getText();
        Long rewordAsLong = Long.parseLong(reward);
        observable.notify(Event.MitReward, rewordAsLong);

        String windowSize = tfMICTWindowSizeValue.getText();
        Long windowSizeAsLong = Long.parseLong(windowSize);
        observable.notify(Event.MitWindowSize, windowSizeAsLong);
      }
    });
    pnMostIntCallerTmp.add(btnMostIntCallerSetNewRule);

    JLabel label_11 = new JLabel("Window Size");
    label_11.setForeground(Color.WHITE);
    label_11.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_11.setBounds(6, 150, 95, 23);
    pnMostIntCallerTmp.add(label_11);

    tfMICTWindowSizeValue = new JTextField();
    tfMICTWindowSizeValue.setText("2");
    tfMICTWindowSizeValue.setColumns(10);
    tfMICTWindowSizeValue.setBounds(101, 144, 134, 28);
    pnMostIntCallerTmp.add(tfMICTWindowSizeValue);

    JLabel label_12 = new JLabel("Seconds");
    label_12.setForeground(Color.WHITE);
    label_12.setBounds(234, 153, 73, 16);
    pnMostIntCallerTmp.add(label_12);

    JLabel label_13 = new JLabel("Hours");
    label_13.setForeground(Color.WHITE);
    label_13.setBounds(234, 110, 73, 16);
    pnMostIntCallerTmp.add(label_13);

    JLabel label_20 = new JLabel("Hours");
    label_20.setForeground(Color.WHITE);
    label_20.setBounds(234, 70, 73, 16);
    pnMostIntCallerTmp.add(label_20);

    JLabel label_1 = new JLabel("Worker");
    label_1.setForeground(Color.WHITE);
    label_1.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_1.setBounds(6, 186, 95, 23);
    pnMostIntCallerTmp.add(label_1);

    tfMITWorkerValue = new JTextField();
    tfMITWorkerValue.setText("10");
    tfMITWorkerValue.setColumns(10);
    tfMITWorkerValue.setBounds(101, 183, 134, 28);
    pnMostIntCallerTmp.add(tfMITWorkerValue);

    JLabel label_10 = new JLabel("Threads");
    label_10.setForeground(Color.WHITE);
    label_10.setBounds(234, 189, 73, 16);
    pnMostIntCallerTmp.add(label_10);

    JPanel pnMostLocalCallerTmp = new JPanel();
    pnMostLocalCallerTmp.setLayout(null);
    pnMostLocalCallerTmp.setBackground(Color.DARK_GRAY);
    GridBagConstraints gbc_pnMostLocalCallerTmp = new GridBagConstraints();
    gbc_pnMostLocalCallerTmp.insets = new Insets(0, 0, 0, 5);
    gbc_pnMostLocalCallerTmp.fill = GridBagConstraints.BOTH;
    gbc_pnMostLocalCallerTmp.gridx = 0;
    gbc_pnMostLocalCallerTmp.gridy = 2;
    templateSettingPanel.add(pnMostLocalCallerTmp, gbc_pnMostLocalCallerTmp);

    JCheckBox cbMostLocalEnable = new JCheckBox("Enabled");
    cbMostLocalEnable.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Boolean isEnabled = e.getStateChange() == ItemEvent.SELECTED;
        observable.notify(Event.MostLocalCallerEnable, isEnabled);
      }
    });
    cbMostLocalEnable.setSelected(true);
    cbMostLocalEnable.setForeground(Color.WHITE);
    cbMostLocalEnable.setFont(new Font("Georgia", Font.PLAIN, 14));
    cbMostLocalEnable.setBounds(365, 6, 90, 23);
    pnMostLocalCallerTmp.add(cbMostLocalEnable);

    tfMLCTDurationValue = new JTextField();
    tfMLCTDurationValue.setText("10");
    tfMLCTDurationValue.setColumns(10);
    tfMLCTDurationValue.setBounds(101, 64, 134, 28);
    pnMostLocalCallerTmp.add(tfMLCTDurationValue);

    tfMLCTRewordValue = new JTextField();
    tfMLCTRewordValue.setText("2");
    tfMLCTRewordValue.setColumns(10);
    tfMLCTRewordValue.setBounds(101, 104, 134, 28);
    pnMostLocalCallerTmp.add(tfMLCTRewordValue);

    JLabel lblMLCTDuration = new JLabel("Duration");
    lblMLCTDuration.setForeground(Color.WHITE);
    lblMLCTDuration.setFont(new Font("Georgia", Font.PLAIN, 14));
    lblMLCTDuration.setBounds(6, 67, 80, 23);
    pnMostLocalCallerTmp.add(lblMLCTDuration);

    JLabel label_22 = new JLabel("Reword");
    label_22.setForeground(Color.WHITE);
    label_22.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_22.setBounds(6, 102, 80, 23);
    pnMostLocalCallerTmp.add(label_22);

    JLabel lblMostLocalCaller = new JLabel("Most Local Caller Template");
    lblMostLocalCaller.setForeground(Color.WHITE);
    lblMostLocalCaller.setFont(new Font("Georgia", Font.PLAIN, 16));
    lblMostLocalCaller.setBounds(6, 6, 229, 23);
    pnMostLocalCallerTmp.add(lblMostLocalCaller);

    JButton btnMostLocalSetNewRule = new JButton("Set New Rule");
    btnMostLocalSetNewRule.setBounds(247, 5, 117, 29);
    btnMostLocalSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String duration = tfMLCTDurationValue.getText();
        Long durationAsLong = Long.parseLong(duration);
        observable.notify(Event.MltDuration, durationAsLong);

        String reward = tfMLCTRewordValue.getText();
        Long rewordAsLong = Long.parseLong(reward);
        observable.notify(Event.MltReward, rewordAsLong);

        String windowSize = tfMLCTWindowSizeValue.getText();
        Long windowSizeAsLong = Long.parseLong(windowSize);
        observable.notify(Event.MltWindowSize, windowSizeAsLong);
      }
    });
    pnMostLocalCallerTmp.add(btnMostLocalSetNewRule);

    JLabel label_24 = new JLabel("Window Size");
    label_24.setForeground(Color.WHITE);
    label_24.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_24.setBounds(6, 150, 95, 23);
    pnMostLocalCallerTmp.add(label_24);

    tfMLCTWindowSizeValue = new JTextField();
    tfMLCTWindowSizeValue.setText("2");
    tfMLCTWindowSizeValue.setColumns(10);
    tfMLCTWindowSizeValue.setBounds(101, 144, 134, 28);
    pnMostLocalCallerTmp.add(tfMLCTWindowSizeValue);

    JLabel label_25 = new JLabel("Seconds");
    label_25.setForeground(Color.WHITE);
    label_25.setBounds(234, 153, 73, 16);
    pnMostLocalCallerTmp.add(label_25);

    JLabel label_26 = new JLabel("Hours");
    label_26.setForeground(Color.WHITE);
    label_26.setBounds(234, 110, 73, 16);
    pnMostLocalCallerTmp.add(label_26);

    JLabel label_27 = new JLabel("Hours");
    label_27.setForeground(Color.WHITE);
    label_27.setBounds(234, 70, 73, 16);
    pnMostLocalCallerTmp.add(label_27);

    JLabel label_16 = new JLabel("Worker");
    label_16.setForeground(Color.WHITE);
    label_16.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_16.setBounds(6, 187, 95, 23);
    pnMostLocalCallerTmp.add(label_16);

    tfMLTWorkerValue = new JTextField();
    tfMLTWorkerValue.setText("10");
    tfMLTWorkerValue.setColumns(10);
    tfMLTWorkerValue.setBounds(101, 184, 134, 28);
    pnMostLocalCallerTmp.add(tfMLTWorkerValue);

    JLabel label_19 = new JLabel("Threads");
    label_19.setForeground(Color.WHITE);
    label_19.setBounds(234, 190, 73, 16);
    pnMostLocalCallerTmp.add(label_19);

    JPanel pnMostNaCallerTmp = new JPanel();
    pnMostNaCallerTmp.setLayout(null);
    pnMostNaCallerTmp.setBackground(Color.DARK_GRAY);
    GridBagConstraints gbc_pnMostNaCallerTmp = new GridBagConstraints();
    gbc_pnMostNaCallerTmp.fill = GridBagConstraints.BOTH;
    gbc_pnMostNaCallerTmp.gridx = 1;
    gbc_pnMostNaCallerTmp.gridy = 2;
    templateSettingPanel.add(pnMostNaCallerTmp, gbc_pnMostNaCallerTmp);

    JCheckBox cbMostNatEnable = new JCheckBox("Enabled");
    cbMostNatEnable.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        Boolean isEnabled = e.getStateChange() == ItemEvent.SELECTED;
        observable.notify(Event.MostNatCallerEnable, isEnabled);
      }
    });
    cbMostNatEnable.setSelected(true);
    cbMostNatEnable.setForeground(Color.WHITE);
    cbMostNatEnable.setFont(new Font("Georgia", Font.PLAIN, 14));
    cbMostNatEnable.setBounds(365, 6, 90, 23);
    pnMostNaCallerTmp.add(cbMostNatEnable);

    tfMNCTDurationValue = new JTextField();
    tfMNCTDurationValue.setText("5");
    tfMNCTDurationValue.setColumns(10);
    tfMNCTDurationValue.setBounds(101, 64, 134, 28);
    pnMostNaCallerTmp.add(tfMNCTDurationValue);

    tfMNCTRewordValue = new JTextField();
    tfMNCTRewordValue.setText("1");
    tfMNCTRewordValue.setColumns(10);
    tfMNCTRewordValue.setBounds(101, 104, 134, 28);
    pnMostNaCallerTmp.add(tfMNCTRewordValue);

    JLabel label_14 = new JLabel("Duration");
    label_14.setForeground(Color.WHITE);
    label_14.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_14.setBounds(6, 67, 80, 23);
    pnMostNaCallerTmp.add(label_14);

    JLabel label_15 = new JLabel("Reword");
    label_15.setForeground(Color.WHITE);
    label_15.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_15.setBounds(6, 102, 80, 23);
    pnMostNaCallerTmp.add(label_15);

    JLabel lblMostNationalCaller = new JLabel("Most National Caller Template");
    lblMostNationalCaller.setForeground(Color.WHITE);
    lblMostNationalCaller.setFont(new Font("Georgia", Font.PLAIN, 16));
    lblMostNationalCaller.setBounds(6, 6, 229, 23);
    pnMostNaCallerTmp.add(lblMostNationalCaller);

    btnMostNatCallerSetNewRule = new JButton("Set New Rule");
    btnMostNatCallerSetNewRule.setBounds(247, 5, 117, 29);
    btnMostLocalSetNewRule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String duration = tfMNCTDurationValue.getText();
        Long durationAsLong = Long.parseLong(duration);
        observable.notify(Event.MntDuration, durationAsLong);

        String reward = tfMNCTRewordValue.getText();
        Long rewordAsLong = Long.parseLong(reward);
        observable.notify(Event.MntReward, rewordAsLong);

        String windowSize = tfMNCTWindowSize.getText();
        Long windowSizeAsLong = Long.parseLong(windowSize);
        observable.notify(Event.MntWindowSize, windowSizeAsLong);
      }
    });
    pnMostNaCallerTmp.add(btnMostNatCallerSetNewRule);

    JLabel label_17 = new JLabel("Window Size");
    label_17.setForeground(Color.WHITE);
    label_17.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_17.setBounds(6, 150, 95, 23);
    pnMostNaCallerTmp.add(label_17);

    tfMNCTWindowSize = new JTextField();
    tfMNCTWindowSize.setText("2");
    tfMNCTWindowSize.setColumns(10);
    tfMNCTWindowSize.setBounds(101, 144, 134, 28);
    pnMostNaCallerTmp.add(tfMNCTWindowSize);

    JLabel label_18 = new JLabel("Seconds");
    label_18.setForeground(Color.WHITE);
    label_18.setBounds(234, 153, 73, 16);
    pnMostNaCallerTmp.add(label_18);

    JLabel label_28 = new JLabel("Hours");
    label_28.setForeground(Color.WHITE);
    label_28.setBounds(234, 110, 73, 16);
    pnMostNaCallerTmp.add(label_28);

    JLabel label_29 = new JLabel("Hours");
    label_29.setForeground(Color.WHITE);
    label_29.setBounds(234, 70, 73, 16);
    pnMostNaCallerTmp.add(label_29);

    JLabel label_21 = new JLabel("Worker");
    label_21.setForeground(Color.WHITE);
    label_21.setFont(new Font("Georgia", Font.PLAIN, 14));
    label_21.setBounds(6, 184, 95, 23);
    pnMostNaCallerTmp.add(label_21);

    tfMNTWorkerValue = new JTextField();
    tfMNTWorkerValue.setText("10");
    tfMNTWorkerValue.setColumns(10);
    tfMNTWorkerValue.setBounds(101, 181, 134, 28);
    pnMostNaCallerTmp.add(tfMNTWorkerValue);

    JLabel label_23 = new JLabel("Threads");
    label_23.setForeground(Color.WHITE);
    label_23.setBounds(234, 187, 73, 16);
    pnMostNaCallerTmp.add(label_23);

    JPanel pnLogging = new JPanel();
    tabbedPane.addTab("Log", null, pnLogging, null);
    GridBagLayout gbl_pnLogging = new GridBagLayout();
    gbl_pnLogging.columnWidths = new int[]{0, 0, 2};
    gbl_pnLogging.rowHeights = new int[]{0, 0, 0, 3};
    gbl_pnLogging.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
    gbl_pnLogging.rowWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
    pnLogging.setLayout(gbl_pnLogging);

    JPanel panel = new JPanel();
    panel.setBackground(new Color(192, 192, 192));
    panel.setLayout(null);
    GridBagConstraints gbc_panel = new GridBagConstraints();
    gbc_panel.insets = new Insets(0, 0, 5, 5);
    gbc_panel.fill = GridBagConstraints.BOTH;
    gbc_panel.gridx = 0;
    gbc_panel.gridy = 0;
    pnLogging.add(panel, gbc_panel);

    JLabel lblOverview = new JLabel("Overview");
    lblOverview.setHorizontalAlignment(SwingConstants.CENTER);
    lblOverview.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
    lblOverview.setBounds(154, 6, 104, 16);
    panel.add(lblOverview);

    JLabel lblTotalCdrsTitle = new JLabel("Total CDRs: ");
    lblTotalCdrsTitle.setBounds(6, 57, 87, 16);
    panel.add(lblTotalCdrsTitle);

    JLabel lblTotalUsers = new JLabel("Processing Rate:");
    lblTotalUsers.setBounds(6, 85, 104, 16);
    panel.add(lblTotalUsers);

    JLabel lblTotalCallDrops = new JLabel("Total Call Drops:");
    lblTotalCallDrops.setBounds(6, 113, 118, 16);
    panel.add(lblTotalCallDrops);

    lblTotalCdrsValue = new JLabel("0");
    lblTotalCdrsValue.setBounds(131, 57, 297, 16);
    panel.add(lblTotalCdrsValue);

    lblProceesRate = new JLabel("0");
    lblProceesRate.setBounds(131, 85, 112, 16);
    panel.add(lblProceesRate);

    lblTotalCallDropsValue = new JLabel("0");
    lblTotalCallDropsValue.setBounds(131, 113, 281, 16);
    panel.add(lblTotalCallDropsValue);

    JLabel lblTimeElapsed = new JLabel("Time Elapsed: ");
    lblTimeElapsed.setHorizontalAlignment(SwingConstants.LEFT);
    lblTimeElapsed.setBounds(6, 31, 104, 16);
    panel.add(lblTimeElapsed);

    elapsedTime = new JLabel("0");
    elapsedTime.setBounds(131, 34, 297, 16);
    panel.add(elapsedTime);

    JPanel pnCallDropTable = new JPanel();
    pnCallDropTable.setBorder(new TitledBorder(null, "Call Drop",
                                               TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagConstraints gbc_pnCallDropTable = new GridBagConstraints();
    gbc_pnCallDropTable.fill = GridBagConstraints.BOTH;
    gbc_pnCallDropTable.insets = new Insets(0, 0, 5, 0);
    gbc_pnCallDropTable.gridx = 1;
    gbc_pnCallDropTable.gridy = 0;
    pnLogging.add(pnCallDropTable, gbc_pnCallDropTable);
    GridBagLayout gbl_pnCallDropTable = new GridBagLayout();
    gbl_pnCallDropTable.columnWidths = new int[]{0, 0};
    gbl_pnCallDropTable.rowHeights = new int[]{0, 0};
    gbl_pnCallDropTable.columnWeights = new double[]{1.0, Double.MIN_VALUE};
    gbl_pnCallDropTable.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    pnCallDropTable.setLayout(gbl_pnCallDropTable);

    JScrollPane scrollPane_4 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_4 = new GridBagConstraints();
    gbc_scrollPane_4.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_4.gridx = 0;
    gbc_scrollPane_4.gridy = 0;
    pnCallDropTable.add(scrollPane_4, gbc_scrollPane_4);
    callDropTable = new CallDropTable();
    scrollPane_4.setViewportView(callDropTable);

    JPanel pnMostCallerTable = new JPanel();
    pnMostCallerTable.setBorder(new TitledBorder(null, "Most Caller",
                                                 TitledBorder.LEADING, TitledBorder.TOP, null,
                                                 null));
    GridBagConstraints gbc_pnMostCallerTable = new GridBagConstraints();
    gbc_pnMostCallerTable.fill = GridBagConstraints.BOTH;
    gbc_pnMostCallerTable.insets = new Insets(0, 0, 5, 5);
    gbc_pnMostCallerTable.gridx = 0;
    gbc_pnMostCallerTable.gridy = 1;
    pnLogging.add(pnMostCallerTable, gbc_pnMostCallerTable);
    GridBagLayout gbl_pnMostCallerTable = new GridBagLayout();
    gbl_pnMostCallerTable.columnWidths = new int[]{0, 0};
    gbl_pnMostCallerTable.rowHeights = new int[]{0, 0};
    gbl_pnMostCallerTable.columnWeights = new double[]{1.0, Double.MIN_VALUE};
    gbl_pnMostCallerTable.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    pnMostCallerTable.setLayout(gbl_pnMostCallerTable);

    JScrollPane scrollPane = new JScrollPane();
    GridBagConstraints gbc_scrollPane = new GridBagConstraints();
    gbc_scrollPane.fill = GridBagConstraints.BOTH;
    gbc_scrollPane.gridx = 0;
    gbc_scrollPane.gridy = 0;
    pnMostCallerTable.add(scrollPane, gbc_scrollPane);

    mostCallerTable = new MostCallerTable();
    scrollPane.setViewportView(mostCallerTable);

    JPanel pnMostIntCallerTable = new JPanel();
    pnMostIntCallerTable.setBorder(new TitledBorder(null,
                                                    "Most International Caller",
                                                    TitledBorder.LEADING, TitledBorder.TOP,
                                                    null, null));
    GridBagConstraints gbc_pnMostIntCallerTable = new GridBagConstraints();
    gbc_pnMostIntCallerTable.fill = GridBagConstraints.BOTH;
    gbc_pnMostIntCallerTable.insets = new Insets(0, 0, 5, 0);
    gbc_pnMostIntCallerTable.gridx = 1;
    gbc_pnMostIntCallerTable.gridy = 1;
    pnLogging.add(pnMostIntCallerTable, gbc_pnMostIntCallerTable);
    GridBagLayout gbl_pnMostIntCallerTable = new GridBagLayout();
    gbl_pnMostIntCallerTable.columnWidths = new int[]{0, 0};
    gbl_pnMostIntCallerTable.rowHeights = new int[]{0, 0};
    gbl_pnMostIntCallerTable.columnWeights = new double[]{1.0,
                                                          Double.MIN_VALUE};
    gbl_pnMostIntCallerTable.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    pnMostIntCallerTable.setLayout(gbl_pnMostIntCallerTable);

    JScrollPane scrollPane_3 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
    gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_3.gridx = 0;
    gbc_scrollPane_3.gridy = 0;
    pnMostIntCallerTable.add(scrollPane_3, gbc_scrollPane_3);

    mostInternationalCallerTable = new MostIntCallerTable();
    scrollPane_3.setViewportView(mostInternationalCallerTable);

    JPanel pnMostNatCallerTable = new JPanel();
    pnMostNatCallerTable.setBorder(new TitledBorder(null,
                                                    "Most National Caller", TitledBorder.LEADING,
                                                    TitledBorder.TOP, null,
                                                    null));
    GridBagConstraints gbc_pnMostNatCallerTable = new GridBagConstraints();
    gbc_pnMostNatCallerTable.fill = GridBagConstraints.BOTH;
    gbc_pnMostNatCallerTable.insets = new Insets(0, 0, 0, 5);
    gbc_pnMostNatCallerTable.gridx = 0;
    gbc_pnMostNatCallerTable.gridy = 2;
    pnLogging.add(pnMostNatCallerTable, gbc_pnMostNatCallerTable);
    GridBagLayout gbl_pnMostNatCallerTable = new GridBagLayout();
    gbl_pnMostNatCallerTable.columnWidths = new int[]{0, 0};
    gbl_pnMostNatCallerTable.rowHeights = new int[]{0, 0};
    gbl_pnMostNatCallerTable.columnWeights = new double[]{1.0,
                                                          Double.MIN_VALUE};
    gbl_pnMostNatCallerTable.rowWeights = new double[]{1.0, Double.MIN_VALUE};
    pnMostNatCallerTable.setLayout(gbl_pnMostNatCallerTable);

    JScrollPane scrollPane_1 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
    gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_1.gridx = 0;
    gbc_scrollPane_1.gridy = 0;
    pnMostNatCallerTable.add(scrollPane_1, gbc_scrollPane_1);

    mostNatCallerTable = new MostNatCallerTable();
    scrollPane_1.setViewportView(mostNatCallerTable);

    JPanel pnMostLocalCallerTable = new JPanel();
    pnMostLocalCallerTable
        .setBorder(new TitledBorder(null, "Most Local Caller",
                                    TitledBorder.LEADING, TitledBorder.TOP,
                                    null, null));
    GridBagConstraints gbc_pnMostLocalCallerTable = new GridBagConstraints();
    gbc_pnMostLocalCallerTable.fill = GridBagConstraints.BOTH;
    gbc_pnMostLocalCallerTable.gridx = 1;
    gbc_pnMostLocalCallerTable.gridy = 2;
    pnLogging.add(pnMostLocalCallerTable, gbc_pnMostLocalCallerTable);
    GridBagLayout gbl_pnMostLocalCallerTable = new GridBagLayout();
    gbl_pnMostLocalCallerTable.columnWidths = new int[]{0, 0};
    gbl_pnMostLocalCallerTable.rowHeights = new int[]{0, 0};
    gbl_pnMostLocalCallerTable.columnWeights = new double[]{1.0,
                                                            Double.MIN_VALUE};
    gbl_pnMostLocalCallerTable.rowWeights = new double[]{1.0,
                                                         Double.MIN_VALUE};
    pnMostLocalCallerTable.setLayout(gbl_pnMostLocalCallerTable);

    JScrollPane scrollPane_2 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
    gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_2.gridx = 0;
    gbc_scrollPane_2.gridy = 0;
    pnMostLocalCallerTable.add(scrollPane_2, gbc_scrollPane_2);

    mostLocalCallerTable = new MostLocalCallerTable();
    scrollPane_2.setViewportView(mostLocalCallerTable);

    JPanel pnGraph = new JPanel();
    pnGraph.setBackground(Color.WHITE);
    tabbedPane.addTab("Graph", null, pnGraph, null);
    pnGraph.setLayout(new BorderLayout(0, 0));

    JLabel lblGraph = new JLabel("Graph");
    pnGraph.add(lblGraph, BorderLayout.NORTH);

    canvasPanel = new CanvasPanel();
    canvasPanel.setBackground(Color.WHITE);
    pnGraph.add(canvasPanel, BorderLayout.CENTER);

    JMenuBar menuBar = new JMenuBar();
    frame.setJMenuBar(menuBar);

    JMenu mnFile = new JMenu("File");
    menuBar.add(mnFile);

    JMenuItem mntmStart = new JMenuItem("Start");
    mntmStart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        EventQueue.invokeLater(new Runnable() {
          public void run() {
            try {
              CdrMainGUI.this.observable.notify(Event.StartTopology,
                                                getConfiguration());
              tfCommonSpaceSize.setEnabled(false);
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        });
      }
    });
    mnFile.add(mntmStart);

    JMenuItem mntmStop = new JMenuItem("Stop");
    mntmStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        tfCommonSpaceSize.setEnabled(true);
        CdrMainGUI.this.observable.notify(Event.StopTopology, null);
        executor.shutdown();
        isStarted = false;
      }
    });
    mnFile.add(mntmStop);

    JMenuItem mntmExit = new JMenuItem("Exit");
    mnFile.add(mntmExit);

    JMenu mnSpout = new JMenu("Spout");
    menuBar.add(mnSpout);

    JRadioButtonMenuItem rdbtnmntmFile = new JRadioButtonMenuItem("File");
    btnGpCdrSrc.add(rdbtnmntmFile);
    mnSpout.add(rdbtnmntmFile);

    JRadioButtonMenuItem rdbtnmntmStream = new JRadioButtonMenuItem("Stream");
    btnGpCdrSrc.add(rdbtnmntmStream);
    mnSpout.add(rdbtnmntmStream);
  }

  @Override
  public void update(Event event, Object data) {
    switch (event) {
      case TupleCount:
        if (isStarted == false) {
          executor.submit(new Runnable() {
            @Override
            public void run() {
              isStarted = true;
              while (true) {
                try {
                  DateTime.now(DateTimeZone.UTC);
                  expired = (DateTime.now(DateTimeZone.UTC).getMillis() - startTime
                      .getMillis()) / 1000;
                  elapsedTime.setText(String.valueOf(expired) + " Seconds");
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
          });
        }
        totalTupleCount += (Long) data;
        lblTotalCdrsValue.setText(String.valueOf(totalTupleCount));
        Long processRate = totalTupleCount / expired;
        lblProceesRate.setText(String.valueOf(processRate) + " tuples/sec");
        break;
      case TotalCallDrop:
        Integer numOfCallDrops = (Integer) data;
        Double percentage = ((numOfCallDrops * 1.0) / totalTupleCount) * 100.0;
        String printedText = String.format("%d (%f)%%", numOfCallDrops,
                                           percentage);
        lblTotalCallDropsValue.setText(printedText);

        // float callQuality = (totalTupleCount - (float)numOfCallDrops) /
        // (float)totalTupleCount;
        // canvasPanel.addCallQualityIndex(callQuality);
        // canvasPanel.repaint();
        break;

      case CallDropList:
        final List<String[]> dataFromBolt = (List<String[]>) data;
        lblProceesRate.setText(String.valueOf(dataFromBolt.size()));
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            callDropTable.updateTable(dataFromBolt);
          }
        });
        break;

      case MostCallerList:
        final List<String[]> dataFromBolt1 = (List<String[]>) data;
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            mostCallerTable.updateTable(dataFromBolt1);
          }
        });
        break;

      case MostIntCallerList:
        final List<String[]> dataFromBolt2 = (List<String[]>) data;
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            mostInternationalCallerTable.updateTable(dataFromBolt2);
          }
        });
        break;

      case MostNatCallerList:
        final List<String[]> dataFromBolt3 = (List<String[]>) data;
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            mostNatCallerTable.updateTable(dataFromBolt3);
          }
        });
        break;

      case MostLocalCallerList:
        final List<String[]> dataFromBolt4 = (List<String[]>) data;
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            mostLocalCallerTable.updateTable(dataFromBolt4);
          }
        });
        break;
    }

  }

  public Map<String, Long> getConfiguration() {
    Map<String, Long> configurationMap = new HashMap<String, Long>();
    configurationMap.put("common-space-size",
                         Long.valueOf(tfCommonSpaceSize.getText()));
    configurationMap.put("common-user-to-display",
                         Long.valueOf(tfUserToDisplayValue.getText()));
    configurationMap.put("common-worker-processes",
                         Long.valueOf(tfWorkerProcesses.getText()));
    configurationMap.put("common-deduplicate-window-size",
                         Long.valueOf(tfDeduplicateWindowSize.getText()));

    configurationMap.put("cdt-call-drop",
                         Long.valueOf(tfCDTCallDropValue.getText()));
    configurationMap.put("cdt-window-size",
                         Long.valueOf(tfCDTWindowSizeValue.getText()));
    configurationMap
        .put("cdt-worker", Long.valueOf(tfCDTWorkerValue.getText()));

    configurationMap.put("mct-duration",
                         Long.valueOf(tfMCTDurationValue.getText()));
    configurationMap
        .put("mct-reword", Long.valueOf(tfMCTRewordValue.getText()));
    configurationMap.put("mct-window",
                         Long.valueOf(tfMCTWindowSizeValue.getText()));
    configurationMap
        .put("mct-worker", Long.valueOf(tfMCTWorkerValue.getText()));

    configurationMap.put("mit-duration",
                         Long.valueOf(tfMICTDurationValue.getText()));
    configurationMap.put("mit-reword",
                         Long.valueOf(tfMICTRewordValue.getText()));
    configurationMap.put("mit-window",
                         Long.valueOf(tfMICTWindowSizeValue.getText()));
    configurationMap
        .put("mit-worker", Long.valueOf(tfMITWorkerValue.getText()));

    configurationMap.put("mnt-duration",
                         Long.valueOf(tfMNCTDurationValue.getText()));
    configurationMap.put("mnt-reword",
                         Long.valueOf(tfMNCTRewordValue.getText()));
    configurationMap
        .put("mnt-window", Long.valueOf(tfMNCTWindowSize.getText()));
    configurationMap
        .put("mnt-worker", Long.valueOf(tfMNTWorkerValue.getText()));

    configurationMap.put("mlt-duration",
                         Long.valueOf(tfMNCTDurationValue.getText()));
    configurationMap.put("mlt-reword",
                         Long.valueOf(tfMLCTRewordValue.getText()));
    configurationMap.put("mlt-window",
                         Long.valueOf(tfMLCTWindowSizeValue.getText()));
    configurationMap
        .put("mlt-worker", Long.valueOf(tfMLTWorkerValue.getText()));

    return configurationMap;
  }
}
