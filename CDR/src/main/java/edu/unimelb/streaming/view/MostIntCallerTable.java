package edu.unimelb.streaming.view;

import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class MostIntCallerTable extends JTable{

  String[] colName = { "IMEI", "IMSI", "Duration (Minutes)"};

  public MostIntCallerTable() {
    DefaultTableModel model = (DefaultTableModel) getModel();
    model.setColumnIdentifiers(colName);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  public void updateTable(List<String[]> callDrops){
    DefaultTableModel model = (DefaultTableModel) getModel();
    model.setRowCount(0);

    for (String[] callDrop : callDrops) {
      String[] data = new String[3];
      data[0] = callDrop[0];
      data[1] = callDrop[1];
      data[2] = callDrop[2];

      model.addRow(data);
    }
    setModel(model);
  }
}
