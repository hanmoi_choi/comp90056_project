package edu.unimelb.streaming.spout;

import java.io.Serializable;

public class CdrForCallDrop implements Serializable {

  String IMEI;
  String IMSI;
  int droppedCallCount;

  public CdrForCallDrop(String IMEI, String IMSI, int droppedCallCount) {
    this.IMEI = IMEI;
    this.IMSI = IMSI;
  }

  @Override
  public String toString() {
    return "CustomerDetails{" +
           "IMEI='" + IMEI + '\'' +
           ", IMSI='" + IMSI + '\'' +
           '}';
  }

  public String getIMEI() {
    return IMEI;
  }

  public void setIMEI(String IMEI) {
    this.IMEI = IMEI;
  }

  public String getIMSI() {
    return IMSI;
  }

  public void setIMSI(String IMSI) {
    this.IMSI = IMSI;
  }

  public int getDroppedCallCount() {
    return droppedCallCount;
  }

  public void setDroppedCallCount(int droppedCallCount) {
    this.droppedCallCount = droppedCallCount;
  }
}
