package edu.unimelb.streaming.spout;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class CdrTextFileReaderSpout extends BaseRichSpout {

  private SpoutOutputCollector collector;
  private FileReader fileReader;
  private boolean completed = false;

  public void ack(Object msgId) {
    System.out.println("OK:" + msgId);
  }
  public void close() {
  }

  public void fail(Object msgId) {
    System.out.println("FAIL:" + msgId);
  }

  /**
   * The only thing that the methods will do It is emit each file line
   */
  public void nextTuple() {
    /**
     * The nextuple it is called forever, so if we have been readed the file we
     * will wait and then return
     */
    if (completed) {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return;
    }
    String str;
    // Open the reader
    BufferedReader reader = new BufferedReader(fileReader);
    try {
      // Read all lines
      int i = 0;
      while ((str = reader.readLine()) != null) {
        String splitedStr[] = str.split(",");
        for (String element : splitedStr) {
          element = element.replace("\"", "");
        }
        Cdr cdr = new Cdr(splitedStr);

        // TODO(Daniel) shall be removed later
        // This code is to give some delay.
        try {
          Thread.sleep(5);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        this.collector.emit(new Values(cdr, cdr.getImei()));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Error reading tuple", e);
    } finally {
      completed = true;
    }
  }

  /**
   * We will create the file and get the collector object
   */
  public void open(Map conf, TopologyContext context,
                   SpoutOutputCollector collector) {
    try {
      String absolutePath = null;
      URL resource = getClass().getClassLoader().getResource(
          conf.get("cdrFile").toString());
      absolutePath = URLDecoder.decode(resource.getFile(), "utf-8");
      this.fileReader = new FileReader(absolutePath);
    } catch (FileNotFoundException e) {
      throw new RuntimeException("Error reading file [" + conf.get("wordFile")
                                 + "]");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    this.collector = collector;
  }

  /**
   * Declare the output field "word"
   */
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("cdr-object", "imei"));
  }
}
