package edu.unimelb.streaming.spout;

import java.io.Serializable;

public class Cdr implements Serializable {
	private String	cdrType;
	private String	imsi;
	private String	imei;
	private String	callingNumber;
	private String	calledNumber;
	private String	recordingEntity;
	private String	location;
	private String	callReference;
	private String	callDuration;
	private String	answerTime;
	private String	seizureTime;
	private String	releaseTime;
	private String	causeForTermination;
	private String	basicService;
	private String	mscAddress;

	public Cdr(String[] splitedStr) {
		cdrType = splitedStr[0];
		imsi = splitedStr[1];
		imei = splitedStr[2];
		callingNumber = splitedStr[3];
		calledNumber = splitedStr[4];
		recordingEntity = splitedStr[5];
		location = splitedStr[6];
		callReference = splitedStr[7];
		callDuration = splitedStr[8];
		answerTime = splitedStr[9];
		seizureTime = splitedStr[10];
		releaseTime = splitedStr[11];
		causeForTermination = splitedStr[12];
		basicService = splitedStr[13];
		mscAddress = splitedStr[14];
	}

	public String getCdrType() {
		return cdrType;
	}

	public void setCdrType(String cdrType) {
		this.cdrType = cdrType;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCallingNumber() {
		return callingNumber;
	}

	public void setCallingNumber(String callingNumber) {
		this.callingNumber = callingNumber;
	}

	public String getCalledNumber() {
		return calledNumber;
	}

	public void setCalledNumber(String calledNumber) {
		this.calledNumber = calledNumber;
	}

	public String getRecordingEntity() {
		return recordingEntity;
	}

	public void setRecordingEntity(String recordingEntity) {
		this.recordingEntity = recordingEntity;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCallReference() {
		return callReference;
	}

	public void setCallReference(String callReference) {
		this.callReference = callReference;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getAnswerTime() {
		return answerTime;
	}

	public void setAnswerTime(String answerTime) {
		this.answerTime = answerTime;
	}

	public String getSeizureTime() {
		return seizureTime;
	}

	public void setSeizureTime(String seizureTime) {
		this.seizureTime = seizureTime;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}

	public String getCauseForTermination() {
		return causeForTermination;
	}

	public void setCauseForTermination(String causeForTermination) {
		this.causeForTermination = causeForTermination;
	}

	public String getBasicService() {
		return basicService;
	}

	public void setBasicService(String basicService) {
		this.basicService = basicService;
	}

	public String getMscAddress() {
		return mscAddress;
	}

	public void setMscAddress(String mscAddress) {
		this.mscAddress = mscAddress;
	}


    public String toIDString() {
        return "" +callingNumber + " " + calledNumber + " " + answerTime + " " ;
    }

    @Override
	public String toString() {
		return "Cdr{" + "cdrType='" + cdrType + '\'' + ", imsi='" + imsi + '\''
				+ ", imei='" + imei + '\'' + ", callingNumber='" + callingNumber + '\''
				+ ", calledNumber='" + calledNumber + '\'' + ", recordingEntity='"
				+ recordingEntity + '\'' + ", location='" + location + '\''
				+ ", callReference='" + callReference + '\'' + ", callDuration='"
				+ callDuration + '\'' + ", answerTime='" + answerTime + '\''
				+ ", seizureTime='" + seizureTime + '\'' + ", releaseTime='"
				+ releaseTime + '\'' + ", causeForTermination='" + causeForTermination
				+ '\'' + ", basicService='" + basicService + '\'' + ", mscAddress='"
				+ mscAddress + '\'' + '}';
	}


}
