package edu.unimelb.streaming.spout;

import java.io.Serializable;

public class CustomerDetails implements Serializable {

  public CustomerDetails(String IMEI, String IMSI) {
    this.IMEI = IMEI;
    this.IMSI = IMSI;
  }

  @Override
  public String toString() {
    return "CustomerDetails{" +
           "IMEI='" + IMEI + '\'' +
           ", IMSI='" + IMSI + '\'' +
           '}';
  }

  String IMEI;
  String IMSI;
  String droppedCallCount;

  public Float getCallDuration() {
    return callDuration;
  }

  public void setCallDuration(Float callDuration) {
    this.callDuration = callDuration;
  }

  Float callDuration;

  public String getIMEI() {
    return IMEI;
  }

  public CustomerDetails(String IMEI, String IMSI, Float callDuration) {
    this.IMEI = IMEI;
    this.IMSI = IMSI;
    this.callDuration = callDuration;
  }

  public void setIMEI(String IMEI) {
    this.IMEI = IMEI;
  }

  public String getIMSI() {
    return IMSI;
  }

  public void setIMSI(String IMSI) {
    this.IMSI = IMSI;
  }

  public String getDroppedCallCount() {
    return droppedCallCount;
  }

  public void setDroppedCallCount(String droppedCallCount) {
    this.droppedCallCount = droppedCallCount;
  }


}
