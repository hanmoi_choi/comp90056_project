package edu.unimelb.streaming.bolt;

import java.util.Map;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observable;

public class TupleCountBolt extends BaseBasicBolt {
  private Observable observable;
  private long totalCount;

  public void setObservable(Observable observable) {
    this.observable = observable;
  }

  @Override
  public void prepare(Map stormConf, TopologyContext context) {
    observable = CdrTemplateController.getInstance();
    totalCount = 0;
    super.prepare(stormConf, context);
  }

  @Override
  public void execute(Tuple tuple, BasicOutputCollector collector) {

    totalCount += 1;
    observable.notify(Event.TupleCount, totalCount);
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer ofd) {
  }

}
