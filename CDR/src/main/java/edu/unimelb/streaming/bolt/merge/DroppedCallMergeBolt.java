package edu.unimelb.streaming.bolt.merge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.CdrForCallDrop;
import edu.unimelb.streaming.util.ValueComparatorForCallDrop;

public class DroppedCallMergeBolt extends BaseRichBolt implements Observer {
  private OutputCollector oc;
  private boolean isEnabled;
  private Long calldropThreshHold;
  private Map<String, CdrForCallDrop> cdrForCallDropHashMap;
  private CdrTemplateController observable;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    calldropThreshHold = (Long) map.get("cdt-call-drop") * 60;
    isEnabled = true;
    oc = outputCollector;
    observable = CdrTemplateController.getInstance();
    observable.register(Event.SetCallDropThreshhold, this);
    cdrForCallDropHashMap = new HashMap<String, CdrForCallDrop>();
  }

  @Override
  public void execute(Tuple tuple) {
    List<CdrForCallDrop> listFromTuple = (List<CdrForCallDrop>) tuple.getValue(0);
    long numberOfTuple = tuple.getLong(1);

    for(CdrForCallDrop cdrForCallDrop : listFromTuple){
      cdrForCallDropHashMap.put(cdrForCallDrop.getIMEI(), cdrForCallDrop);
    }

    List<String[]> resultList = new ArrayList<String[]>();
    Integer sum = 0;
    ValueComparatorForCallDrop bvc = new ValueComparatorForCallDrop(cdrForCallDropHashMap);
    TreeMap<String, CdrForCallDrop> sortMap = new TreeMap<String, CdrForCallDrop>(bvc);
    sortMap.putAll(cdrForCallDropHashMap);
    Set<Map.Entry<String, CdrForCallDrop>> entries = sortMap.entrySet();
    Iterator<Map.Entry<String, CdrForCallDrop>> entryIterator = entries.iterator();

    while (entryIterator.hasNext()) {
      Map.Entry<String, CdrForCallDrop> entry = entryIterator.next();
      CdrForCallDrop value = entry.getValue();
      int droppedCallCount = value.getDroppedCallCount();
      String[] result = new String[]{value.getIMEI(), value.getIMSI(),
                                     String.valueOf(droppedCallCount)};

      if(droppedCallCount > calldropThreshHold){
        observable.notify(Event.SendCallDropEmail, new String[]{value.getIMEI(), String.valueOf(droppedCallCount)});
      }

      resultList.add(result);
      sum += droppedCallCount;

      if (droppedCallCount == 0) {
        break;
      }
    }

    if(resultList.size() > 0) {
      observable.notify(Event.TotalCallDrop, sum);
      observable.notify(Event.CallDropList, resultList);
    }

    observable.notify(Event.TupleCount, numberOfTuple);
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case SetCallDropThreshhold:
        calldropThreshHold = (Long) data * 60;
        break;
    }
  }
}
