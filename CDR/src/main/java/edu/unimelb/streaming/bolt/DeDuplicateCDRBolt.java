package edu.unimelb.streaming.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.Cdr;
import edu.unimelb.streaming.spout.CustomerDetails;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DeDuplicateCDRBolt extends BaseRichBolt {

    OutputCollector oc;
    Set<String> dupList;
    int numElem = 10000;

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        oc = outputCollector;
        dupList = new HashSet<String>();
    }

    @Override
    public void execute(Tuple tuple) {
        Cdr cdr = (Cdr) tuple.getValue(0);
        String cdrStrVal = new String(cdr.toIDString());
        //System.out.println("byte size of cdr = " + cdrStrVal.getBytes().length);
        //System.out.println("cdr strval is " + cdrStrVal);
        //printList();

        if(!dupList.contains(cdrStrVal)){
            // doesnt contain, send it and add to list

            // first check if list is full, flush it
     //       System.out.println("list size is " + dupList.size());
            if(dupList.size() >= numElem){
                dupList.clear();
                //System.out.println("list cleared");
            }
            dupList.add(cdrStrVal);
            oc.emit(new Values(cdr, cdr.getImei()));
            oc.ack(tuple);
        }
    }

    public void printList(){
        for (String s : dupList){
            System.out.print(" " + s + ", ");
        }
        System.out.println("");
    }
    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("cdr-object", "imei"));
    }

}
