package edu.unimelb.streaming.bolt;

import com.google.common.base.CharMatcher;

import java.util.HashSet;
import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.tools.StopWorldRemoval;

public class FilterForSimiliaritiesBolt extends BaseRichBolt {

  private static final long serialVersionUID = 1L;
  private OutputCollector collector;
  private HashSet<String> stopSet;

  @Override
  public void prepare(Map stormConf, TopologyContext context,
                      OutputCollector collector) {
    this.collector = collector;
    stopSet = StopWorldRemoval.createStopWordSet();
  }

  @Override
  public void execute(Tuple tuple) {
    String userScreenName = tuple.getString(0);
    String message = tuple.getString(1);
    HashSet<String> messagesTokened = new HashSet<String>();

    String newMessage = CharMatcher.ASCII.retainFrom(message);
    String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
    newMessage = newMessage.replaceAll(regex, "");
    newMessage = newMessage.replaceAll("[+&/%?=~_()|!:,.;]", "");

    String[] word = newMessage.toLowerCase().split(" ");
    for (String w : word) {
      if (!stopSet.contains(w)) {
        w = CharMatcher.WHITESPACE.and(CharMatcher.BREAKING_WHITESPACE)
            .trimFrom(w);
        if (w.length() >= 2 && w.matches("\\d+") == false
            && w.matches("@.+") == false) {
          messagesTokened.add(w);
        }
      }
    }
    collector.emit(new Values(userScreenName, messagesTokened));
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("user-screenname",
                                            "messages-tokened"));
  }
}
