package edu.unimelb.streaming.bolt.algorithm;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.CustomerDetails;
import edu.unimelb.streaming.util.ValueComparator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static edu.unimelb.streaming.controller.Event.SetCallDropThreshhold;

public class SpaceSavingCDRMostIntlCallersBolt extends BaseRichBolt implements Observer {

  private static final int NUM_OF_THREADS = 1;
  OutputCollector oc;
  Long k;
  int numkeys;
  ConcurrentHashMap<String, CustomerDetails> spaceMap;
  private CdrTemplateController observable;
  private ExecutorService executor;
  private Long sleepTime;
  private long numberOfTuple;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    numkeys = 0;
    numberOfTuple = 0;
    k = (Long)map.get("common-space-size");
    oc = outputCollector;
    observable = CdrTemplateController.getInstance();
    observable.register(Event.MitWindowSize, this);
    this.observable.register(Event.StopTopology, this);
    spaceMap = new ConcurrentHashMap<String, CustomerDetails>();
    sleepTime = (Long)map.get("mit-window") * 1000;
    executor = Executors.newFixedThreadPool(1);

    executor.execute(new Runnable() {
      @Override
      public void run() {

        while (true) {
          try {
            ValueComparator bvc = new ValueComparator(spaceMap);
            TreeMap<String, CustomerDetails> sortMap = new TreeMap<String, CustomerDetails>(bvc);
            sortMap.putAll(spaceMap);
            Set<Map.Entry<String, CustomerDetails>> set = sortMap.entrySet();
            Iterator<Map.Entry<String, CustomerDetails>> iterator = set.iterator();

            List<CustomerDetails> listToPass = new ArrayList<CustomerDetails>();
            while (iterator.hasNext()) {
              Map.Entry<String, CustomerDetails> entry = iterator.next();
              CustomerDetails value = entry.getValue();
              listToPass.add(value);
            }

            oc.emit(new Values(listToPass, numberOfTuple));
            numberOfTuple = 0;
            Thread.sleep(sleepTime);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  @Override
  public void execute(Tuple tuple) {
    numberOfTuple += 1;
    CustomerDetails cd = (CustomerDetails) tuple.getValue(0);
    addToMapOrReplace(cd);
  }

  private void printMap() {
    System.out.print("{");
    for (String k : spaceMap.keySet()) {
      System.out.print(" k = " + k + " v = " + spaceMap.get(k).getCallDuration() + " ");
    }
    System.out.println("}");
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("user-list", "number-of-tuple-processed"));
  }

  private void addToMapOrReplace(CustomerDetails cd) {
    /******************************/
    Set<String> keySet = spaceMap.keySet();

    //System.out.println("key set is " + (keySet.isEmpty()));
    if ((keySet.contains(cd.getIMEI()))) {
      float count = spaceMap.get(cd.getIMEI()).getCallDuration();
      //System.out.println("count is " + count);
      float incrementAmtSec = cd.getCallDuration();
      float incrementAmt = incrementAmtSec / 60;
      count += incrementAmt;
//            System.out.println("before incrementing " + cd.getIMEI() + " : " + cd.getCallDuration()/60);
      cd.setCallDuration(count);
      spaceMap.put(cd.getIMEI(), cd);
//            System.out.println("After incrementing ");
//            printMap();

    } else {
      // we dont have the caller in our map.
      // enough spaces, add him
//      System.out.println("dont have key " + cd.getIMEI());
      if (numkeys < k) {
        float initDuration = cd.getCallDuration() / 60;
        cd.setCallDuration(initDuration);
        spaceMap.put(cd.getIMEI(), cd);
        numkeys++;
//                System.out.println("added key ====");
        //               printMap();
      } else {
        // not enough spaces, find min count, and replace with min+1.
//        System.out.println("not enough spaces");
        String replaceMe = findMinCaller(spaceMap);
//        System.out.println(
//            "minimum caller is::::: " + replaceMe + " minimum in map is " + spaceMap.get(replaceMe)
//                .getCallDuration());
//        printMap();
        float currMin = spaceMap.get(replaceMe).getCallDuration();
        float newCallDuration = cd.getCallDuration() / 60;
//        System.out.println("new duration = " + newCallDuration + " curr min is " + currMin);
        if (newCallDuration > currMin) {
          cd.setCallDuration(newCallDuration);
          spaceMap.remove(replaceMe);
//          System.out.println("inserted : " + cd.getIMSI() + " : " + cd.getCallDuration() + "....");

          spaceMap.put(cd.getIMEI(), cd);
//          printMap();
          //System.exit(1);
          return;
        }
//        System.out.println("didnt add since mnutes not enough, " + cd.getCallDuration() / 60);
//        System.out.println(currMin + " is larger than " + newCallDuration);

      }
    }
  }

  private String findMinCaller(ConcurrentHashMap<String, CustomerDetails> spaceMap) {

    float currMin = Float.MAX_VALUE;
    String key = null;
    for (String caller : spaceMap.keySet()) {
      float thisCount = spaceMap.get(caller).getCallDuration();
      //printMap();
      //System.out.println("curr min is : " + currMin + "this count is " + thisCount);
      if (thisCount < currMin) {
        //System.out.println("new min");
        key = new String(caller);
        currMin = thisCount;
      }
    }
//    System.out.println("returned " + spaceMap.get(key));
    return key;
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case MitWindowSize:
        sleepTime = (Long) data * 1000;
        break;
      case StopTopology:
        executor.shutdown();
        break;
    }
  }
}
