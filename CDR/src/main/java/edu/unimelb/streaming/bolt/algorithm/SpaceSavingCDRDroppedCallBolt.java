package edu.unimelb.streaming.bolt.algorithm;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.CdrForCallDrop;
import edu.unimelb.streaming.util.ValueComparatorForCallDrop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static edu.unimelb.streaming.controller.Event.SetCallDropThreshhold;

public class SpaceSavingCDRDroppedCallBolt extends BaseRichBolt implements Observer{
  OutputCollector oc;
  Long k;
  int numkeys;
  ConcurrentHashMap<String, CdrForCallDrop> spaceMap;
  private CdrTemplateController observable;
  private ExecutorService executor;
  private Long timeingWindowSize;
  private long numberOfTuple;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    numkeys = 0;
    numberOfTuple = 0L;

    k = (Long)map.get("common-space-size");
    timeingWindowSize = (Long)map.get("cdt-window-size") * 1000;

    oc = outputCollector;
    observable = CdrTemplateController.getInstance();
    observable.register(Event.CallDropWindowSize, this);
    this.observable.register(Event.StopTopology, this);
    spaceMap = new ConcurrentHashMap<String, CdrForCallDrop>();

    executor = Executors.newFixedThreadPool(1);
    executor.execute(new Runnable() {
      @Override
      public void run() {
        while (true) {
          try {
            ValueComparatorForCallDrop bvc = new ValueComparatorForCallDrop(spaceMap);
            TreeMap<String, CdrForCallDrop> sortMap = new TreeMap<String, CdrForCallDrop>(bvc);
            sortMap.putAll(spaceMap);
            Set<Map.Entry<String, CdrForCallDrop>> set = sortMap.entrySet();
            Iterator<Map.Entry<String, CdrForCallDrop>> iterator = set.iterator();

            List<CdrForCallDrop> listToPass = new ArrayList<CdrForCallDrop>();
            while (iterator.hasNext()) {
              Map.Entry<String, CdrForCallDrop> entry = iterator.next();
              CdrForCallDrop value = entry.getValue();
              listToPass.add(value);
              int droppedCallCount = value.getDroppedCallCount();

              if (droppedCallCount == 0) {
                break;
              }
            }
            oc.emit(new Values(listToPass, numberOfTuple));
            numberOfTuple = 0;
            Thread.sleep(timeingWindowSize);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  @Override
  public void execute(Tuple tuple) {
    numberOfTuple += 1;
    CdrForCallDrop cd = (CdrForCallDrop) tuple.getValue(0);
    addToMapOrReplace(cd);
  }

  private void printMap() {
    System.out.print("{");
    for (String k : spaceMap.keySet()) {
      System.out.print(" k = " + k.toString() + " v = " + spaceMap.get(k) + " ");
    }
    System.out.println("}");
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("user-list", "number-of-tuple-processed"));
  }

  private void addToMapOrReplace(CdrForCallDrop cd) {
    if (spaceMap.containsKey(cd.getIMEI())){
      CdrForCallDrop toUpdate = spaceMap.get(cd.getIMEI());
      toUpdate.setDroppedCallCount(toUpdate.getDroppedCallCount() + 1);
      spaceMap.put(cd.getIMEI(), toUpdate);
    } else {
      // we dont have the caller in our map.
      // enough spaces, add him
      if (numkeys < k) {
        cd.setDroppedCallCount(1);
        spaceMap.put(cd.getIMEI(), cd);
        numkeys++;
      } else {
        // not enough spaces, find min count, and replace with min+1.
        String replaceMe = findMinCaller(spaceMap);
        // System.out.println("minimum caller is::::: " + replaceMe + " minimum calls in map is " + spaceMap.get(replaceMe).intValue());
        int minCalls = spaceMap.get(replaceMe).getDroppedCallCount();
        spaceMap.remove(replaceMe);
        cd.setDroppedCallCount(minCalls + 1);
        spaceMap.put(cd.getIMEI(), cd);
      }
    }
  }

  private String findMinCaller(ConcurrentHashMap<String, CdrForCallDrop> spaceMap) {
    int currMin = Integer.MAX_VALUE;
    String minCaller = null;
    for (String caller : spaceMap.keySet()) {
      int thisCount = spaceMap.get(caller).getDroppedCallCount();
      if (thisCount < currMin) {
        minCaller = caller;
      }
    }
    return minCaller;
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case CallDropWindowSize:
        timeingWindowSize = (Long) data * 1000;
        break;
      case StopTopology:
        executor.shutdown();
        break;
    }
  }
}
