package edu.unimelb.streaming.bolt;

import edu.unimelb.streaming.spout.Cdr;

public class CdrTemplateAndData {
  public CdrTemplateAndData(Cdr cdr, int templateNum) {
    this.cdr = cdr;
    this.templateNum = templateNum;
  }

  Cdr cdr;
  int templateNum;

  public Cdr getCdr() {
    return cdr;
  }

  public void setCdr(Cdr cdr) {
    this.cdr = cdr;
  }

  @Override
  public String toString() {
    return "CdrTemplateAndData{" +
           "cdr=" + cdr +
           ", templateNum=" + templateNum +
           '}';
  }
}
