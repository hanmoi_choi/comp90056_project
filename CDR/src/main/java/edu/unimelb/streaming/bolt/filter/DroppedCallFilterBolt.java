package edu.unimelb.streaming.bolt.filter;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.Cdr;
import edu.unimelb.streaming.spout.CdrForCallDrop;
import edu.unimelb.streaming.spout.CustomerDetails;

import static edu.unimelb.streaming.controller.Event.DroppedCallEnable;

public class DroppedCallFilterBolt extends BaseRichBolt implements Observer {
  private OutputCollector oc;
  private boolean isEnabled;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    isEnabled = true;
    oc = outputCollector;
    CdrTemplateController observable = CdrTemplateController.getInstance();
    observable.register(DroppedCallEnable, this);
  }

  @Override
  public void execute(Tuple tuple) {
    Cdr cdr = (Cdr) tuple .getValue(0);

    if(!cdr.getCauseForTermination().equals("\"00\"")){
      CdrForCallDrop cd = new CdrForCallDrop(cdr.getImei(), cdr.getImsi(), 0);

      if(isEnabled){
        oc.emit(new Values(cd, cd.getIMEI()));
      }
      oc.ack(tuple);
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("cust-details", "imei"));
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case DroppedCallEnable:
        isEnabled = (Boolean) data;
      break;
    }
  }
}
