package edu.unimelb.streaming.bolt.filter;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.Cdr;
import edu.unimelb.streaming.spout.CustomerDetails;

public class IntlCallFilterBolt extends BaseRichBolt implements Observer {

  OutputCollector oc;
  private Boolean isEnabled;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    oc = outputCollector;
    isEnabled = true;
    CdrTemplateController observable = CdrTemplateController.getInstance();
    observable.register(Event.MostIntCallerEnable, this);
  }

  @Override
  public void execute(Tuple tuple) {
    Cdr cdr = (Cdr) tuple.getValue(0);

    String callingNum = cdr.getCallingNumber();
    String calledNum = cdr.getCalledNumber();
    String CountryCode1 = callingNum.substring(1, 2);
    String CountryCode2 = calledNum.substring(1, 2);
    if ((cdr.getCauseForTermination().equals("\"00\"")) && (!CountryCode1.equals(CountryCode2))) {

      CustomerDetails
          cd =
          new CustomerDetails(cdr.getImei(), cdr.getImsi(),
                              Float.parseFloat(cdr.getCallDuration()));
      if(isEnabled){
        oc.emit(new Values(cd, cd.getIMEI()));
      }

      oc.ack(tuple);

    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("cust-details", "imei"));
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case MostIntCallerEnable:
        isEnabled = (Boolean) data;
        break;
    }
  }
}
