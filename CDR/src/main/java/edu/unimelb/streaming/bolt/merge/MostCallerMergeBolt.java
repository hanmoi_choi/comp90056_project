package edu.unimelb.streaming.bolt.merge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;
import edu.unimelb.streaming.spout.CustomerDetails;
import edu.unimelb.streaming.util.ValueComparator;

import static edu.unimelb.streaming.controller.Event.DroppedCallEnable;

public class MostCallerMergeBolt extends BaseRichBolt implements Observer {
  private Long callDurationForReward;
  private Map<String, CustomerDetails> cdrForCallDropHashMap;
  private CdrTemplateController observable;
  private Long howManyDisplay;


  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    callDurationForReward = (Long) map.get("mct-duration") * 60;
    howManyDisplay = (Long) map.get("common-user-to-display");
    observable = CdrTemplateController.getInstance();
    observable.register(Event.MctCallDuration, this);
    observable.register(Event.HowManyToDisplay, this);
    cdrForCallDropHashMap = new ConcurrentHashMap<String, CustomerDetails>();
  }

  @Override
  public void execute(Tuple tuple) {
    List<CustomerDetails> listFromTuple = (List<CustomerDetails>) tuple.getValue(0);
    long numberOfTuple = tuple.getLong(1);
    for(CustomerDetails cdrForCallDrop : listFromTuple){
      cdrForCallDropHashMap.put(cdrForCallDrop.getIMEI(), cdrForCallDrop);
    }

    List<String[]> resultList = new ArrayList<String[]>();
    Integer sum = 0;
    ValueComparator bvc = new ValueComparator(cdrForCallDropHashMap);
    TreeMap<String, CustomerDetails> sortMap = new TreeMap<String, CustomerDetails>(bvc);
    sortMap.putAll(cdrForCallDropHashMap);
    Set<Map.Entry<String, CustomerDetails>> entries = sortMap.entrySet();
    Iterator<Map.Entry<String, CustomerDetails>> entryIterator = entries.iterator();

    int count = 0;
    while (entryIterator.hasNext()) {
      Map.Entry<String, CustomerDetails> entry = entryIterator.next();
      CustomerDetails value = entry.getValue();
      float callDuration = value.getCallDuration();
      String[] result = new String[]{value.getIMEI(), value.getIMSI(),
                                     String.valueOf(callDuration)};

      if(callDuration > callDurationForReward){
        observable.notify(Event.SendCallDropEmail, new String[]{value.getIMEI(), String.valueOf(callDuration)});
      }

      resultList.add(result);
      count +=1;

      if(count > howManyDisplay) break;
    }

    if(resultList.size() > 0) {
      observable.notify(Event.MostCallerList, resultList);
    }
    observable.notify(Event.TupleCount, numberOfTuple);
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("cust-details"));
  }

  @Override
  public void update(Event event, Object data) {
    switch (event){
      case MctCallDuration:
        callDurationForReward = (Long) data * 60;
        break;
      case HowManyToDisplay:
        howManyDisplay = (Long) data;
        break;
    }
  }
}
