package edu.unimelb.streaming.util;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.controller.Observer;

import static edu.unimelb.streaming.controller.Event.SendCallDropEmail;

public class EmailUtil implements Observer {

  public EmailUtil(CdrTemplateController controller) {
    controller.register(Event.SendCallDropEmail, this);
  }

  private void sendMail(String messageBody) {
    final String username = "forhim185@gmail.com";
    final String password = "Jo@nne09ii";

    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "587");

    Session session = Session.getInstance(props,
                                          new javax.mail.Authenticator() {
                                            protected PasswordAuthentication getPasswordAuthentication() {
                                              return new PasswordAuthentication(
                                                  username, password);
                                            }
                                          });
    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress("forhim185@gmail.com"));
      message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse("forhim185@gmail.com"));
      message.setSubject("Testing Subject");
      message.setText(messageBody);

      Transport.send(message);
    } catch (MessagingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void update(Event event, Object data) {
//    switch (event){
//      case SendCallDropEmail:
//        String [] info = (String[]) data;
//        sendMail(String.format("Dear %s, \n Sorry for your inconvenience. We have been notified "
//                               + "that you had been experienced more that %s times call drop\""
//                               + "we would like to reimbursement it", info[0], info[1]));
//       break;
//    }
  }
}
