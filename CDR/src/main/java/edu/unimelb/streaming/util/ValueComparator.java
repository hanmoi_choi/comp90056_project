package edu.unimelb.streaming.util;

import java.util.Comparator;
import java.util.Map;

import edu.unimelb.streaming.spout.CdrForCallDrop;
import edu.unimelb.streaming.spout.CustomerDetails;

public class ValueComparator implements Comparator<String> {
  Map<String, CustomerDetails> base;

  public ValueComparator(Map<String, CustomerDetails> base) {
    this.base = base;
  }

  // Note: this comparator imposes orderings that are inconsistent with equals.
  public int compare(String a, String b) {
    if(base.get(a) == null || base.get(b) == null){
      return 1;
    }
    if (base.get(a).getCallDuration() >= base.get(b).getCallDuration()) {
      return -1;
    } else {
      return 1;
    } // returning 0 would merge keys
  }
}
