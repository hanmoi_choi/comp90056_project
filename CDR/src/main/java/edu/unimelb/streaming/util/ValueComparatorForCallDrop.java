package edu.unimelb.streaming.util;

import java.util.Comparator;
import java.util.Map;

import edu.unimelb.streaming.spout.CdrForCallDrop;

public class ValueComparatorForCallDrop implements Comparator<String> {
  Map<String, CdrForCallDrop> base;

  public ValueComparatorForCallDrop(Map<String, CdrForCallDrop> base) {
    this.base = base;
  }

  // Note: this comparator imposes orderings that are inconsistent with equals.
  public int compare(String a, String b) {
    if (base.get(a).getDroppedCallCount() >= base.get(b).getDroppedCallCount()) {
      return -1;
    } else {
      return 1;
    } // returning 0 would merge keys
  }
}
