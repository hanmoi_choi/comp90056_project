package edu.unimelb.streaming.topology;

import java.util.HashMap;
import java.util.Map;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import edu.unimelb.streaming.bolt.*;
import edu.unimelb.streaming.bolt.algorithm.SpaceSavingCDRDroppedCallBolt;
import edu.unimelb.streaming.bolt.algorithm.SpaceSavingCDRMostCallersBolt;
import edu.unimelb.streaming.bolt.algorithm.SpaceSavingCDRMostIntlCallersBolt;
import edu.unimelb.streaming.bolt.algorithm.SpaceSavingCDRMostLocalCallersBolt;
import edu.unimelb.streaming.bolt.algorithm.SpaceSavingCDRMostNatCallersBolt;
import edu.unimelb.streaming.bolt.filter.AnyCallFilterBolt;
import edu.unimelb.streaming.bolt.filter.DroppedCallFilterBolt;
import edu.unimelb.streaming.bolt.filter.IntlCallFilterBolt;
import edu.unimelb.streaming.bolt.filter.LocalCallFilterBolt;
import edu.unimelb.streaming.bolt.filter.NatCallFilterBolt;
import edu.unimelb.streaming.bolt.merge.DroppedCallMergeBolt;
import edu.unimelb.streaming.bolt.merge.MostCallerMergeBolt;
import edu.unimelb.streaming.bolt.merge.MostIntlCallerMergeBolt;
import edu.unimelb.streaming.bolt.merge.MostLocalCallerMergeBolt;
import edu.unimelb.streaming.bolt.merge.MostNatCallerMergeBolt;
import edu.unimelb.streaming.controller.*;
import edu.unimelb.streaming.controller.Event;
import edu.unimelb.streaming.spout.CdrTextFileReaderSpout;

import static edu.unimelb.streaming.controller.Event.StartTopology;

public class CdrMarketingTopology implements Observer {

  private static LocalCluster cluster;
  private Observable observable;
  private Config config;
  private Long numOfThreadForSpout;
  private Long numOfThreadForDC;
  private Long numOfThreadForMICT;
  private Long numOfThreadsForMNCT;
  private Long numOfThreadForMLCT;
  private Long numOfThreadForMCT;

  public CdrMarketingTopology(Observable observable) {
    this.observable = observable;
    this.observable.register(StartTopology, this);
    this.observable.register(Event.StopTopology, this);
  }

  public void run() {

    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("TextReaderSpout", new CdrTextFileReaderSpout(), 1);
    TupleCountBolt tupleCountBolt = new TupleCountBolt();


    //DeDuplicate
    builder.setBolt("DeDuplicateCDRBolt", new DeDuplicateCDRBolt(), 1)
        .fieldsGrouping("TextReaderSpout", new Fields("imei"));

    // Dropped Calls
    builder.setBolt("DroppedCallFilter", new DroppedCallFilterBolt(), numOfThreadForDC)
        .fieldsGrouping("DeDuplicateCDRBolt", new Fields("imei"));
    builder.setBolt("SpaceSavingCDRDroppedCallBolt", new SpaceSavingCDRDroppedCallBolt(),
                    numOfThreadForDC)
        .fieldsGrouping("DroppedCallFilter", new Fields("imei"));
    builder.setBolt("DroppedCallMergeBolt", new DroppedCallMergeBolt(), 1)
        .fieldsGrouping("SpaceSavingCDRDroppedCallBolt", new Fields("user-list"));

    // International Calls
    builder.setBolt("IntlCallFilter", new IntlCallFilterBolt(), numOfThreadForMICT)
        .fieldsGrouping("DeDuplicateCDRBolt", new Fields("imei"));
    builder.setBolt("SpaceSavingCDRMostIntlCallers", new SpaceSavingCDRMostIntlCallersBolt(),
                    numOfThreadForMICT)
        .fieldsGrouping("IntlCallFilter", new Fields("imei"));
    builder.setBolt("MostIntlCallerMergeBolt", new MostIntlCallerMergeBolt(), 1).fieldsGrouping(
        "SpaceSavingCDRMostIntlCallers", new Fields("user-list"));

    // National Calls
    builder.setBolt("NatCallFilter", new NatCallFilterBolt(), numOfThreadsForMNCT)
        .fieldsGrouping("DeDuplicateCDRBolt", new Fields("imei"));
    builder.setBolt("SpaceSavingCDRMostNatCallersBolt", new SpaceSavingCDRMostNatCallersBolt(),
                    numOfThreadsForMNCT)
        .fieldsGrouping("NatCallFilter", new Fields("imei"));
    builder.setBolt("MostNatCallerMergeBolt", new MostNatCallerMergeBolt(), 1).fieldsGrouping(
        "SpaceSavingCDRMostNatCallersBolt", new Fields("user-list"));

    // Local Calls
    builder.setBolt("LocalCallFilter", new LocalCallFilterBolt(), numOfThreadForMLCT)
        .fieldsGrouping("DeDuplicateCDRBolt", new Fields("imei"));
    builder
        .setBolt("SpaceSavingCDRMostLocalCallersBolt", new SpaceSavingCDRMostLocalCallersBolt(), 10)
        .fieldsGrouping("LocalCallFilter", new Fields("imei"));
    builder.setBolt("MostLocalCallerMergeBolt", new MostLocalCallerMergeBolt(), 1).fieldsGrouping(
        "SpaceSavingCDRMostLocalCallersBolt", new Fields("user-list"));

    // All calls
    builder.setBolt("AnyCallFilter", new AnyCallFilterBolt(), numOfThreadForMCT)
        .fieldsGrouping("DeDuplicateCDRBolt",
                        new Fields(
                            "imei"));
    builder.setBolt("SpaceSavingCDRMostCallersBolt", new SpaceSavingCDRMostCallersBolt(),
                    numOfThreadForMCT).fieldsGrouping("AnyCallFilter", new Fields("imei"));
    builder.setBolt("MostCallerMergeBolt", new MostCallerMergeBolt(), 1).fieldsGrouping(
        "SpaceSavingCDRMostCallersBolt", new Fields("user-list"));

    cluster = new LocalCluster();
    cluster.submitTopology("cdr-marketing-template", config, builder.createTopology());

  }

  public void stop() {
    if (cluster != null) {
      cluster.killTopology("cdr-marketing-template");
      cluster.shutdown();
    }
  }

  @Override
  public void update(Event event, Object data) {
    switch (event) {
      case StartTopology:
        Map<String, Long> configuration = (Map<String, Long>) data;
        numOfThreadForSpout = configuration.get("common-spout-worker");
        numOfThreadForDC = configuration.get("cdt-worker");
        numOfThreadForMICT = configuration.get("mit-worker");
        numOfThreadsForMNCT = configuration.get("mnt-worker");
        numOfThreadForMLCT = configuration.get("mlt-worker");
        numOfThreadForMCT = configuration.get("mct-worker");
        config.setNumWorkers(configuration.get("common-worker-processes").intValue());
        run();
        break;
      case StopTopology:
        stop();
        break;
    }
  }

  public void setConfiguration(Map<String, Long> configuration) {
    config = new Config();
    config.setDebug(false);
    config.setNumWorkers(configuration.get("common-worker-processes").intValue());
    config.put("cdrFile", "cdr_rawsample1.txt");

    config.put("common-space-size", configuration.get("common-space-size"));
    config.put("common-user-to-display", configuration.get("common-user-to-display"));
    config
        .put("common-deduplicate-window-size", configuration.get("common-deduplicate-window-size"));
    config.put("cdt-call-drop", configuration.get("cdt-call-drop"));
    config.put("cdt-window-size", configuration.get("cdt-window-size"));
    config.put("mct-duration", configuration.get("mct-duration"));
    config.put("mct-reword", configuration.get("mct-reword"));
    config.put("mct-window", configuration.get("mct-window"));
    config.put("mit-duration", configuration.get("mit-duration"));
    config.put("mit-reword", configuration.get("mit-reword"));
    config.put("mit-window", configuration.get("mit-window"));
    config.put("mnt-duration", configuration.get("mnt-duration"));
    config.put("mnt-reword", configuration.get("mnt-reword"));
    config.put("mnt-window", configuration.get("mnt-window"));
    config.put("mlt-duration", configuration.get("mlt-duration"));
    config.put("mlt-reword", configuration.get("mlt-reword"));
    config.put("mlt-window", configuration.get("mlt-window"));
  }
}
