package edu.unimelb.streaming.controller;

public interface Observer {
  void update(Event event, Object data);

}
