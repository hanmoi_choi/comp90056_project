package edu.unimelb.streaming.controller;

public interface Observable {
  void register(Event event, Observer observer);
  void unregister(Event event, Observer observer);
  void notify(Event event, Object data);
}
