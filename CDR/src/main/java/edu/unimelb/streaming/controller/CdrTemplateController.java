package edu.unimelb.streaming.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CdrTemplateController implements Observable {

  private static volatile CdrTemplateController instance = null;
  private Map<Event, List<Observer>> observerMap;

  private CdrTemplateController() {
    observerMap = new HashMap<Event, List<Observer>>();
  }

  public static CdrTemplateController getInstance() {
    if (instance == null) {
      synchronized (CdrTemplateController.class) {
        if (instance == null) {
          instance = new CdrTemplateController();
        }
      }
    }
    return instance;
  }

  @Override
  public void register(Event event, Observer observer) {
    List<Observer> observers = observerMap.get(event);
    if (observers != null) {
      observers.add(observer);
    } else {
      List<Observer> observerList = new ArrayList<Observer>();
      observerList.add(observer);
      observerMap.put(event, observerList);
    }
  }

  @Override
  public void unregister(Event event, Observer observer) {
    List<Observer> observers = observerMap.get(event);
    if (observers != null) {
      observers.remove(observer);
    }
  }

  @Override
  public void notify(Event event, Object data) {
    List<Observer> observers = observerMap.get(event);
    if (observers != null) {
      for (Observer observer : observers) {
        observer.update(event, data);
      }
    }
  }
}
