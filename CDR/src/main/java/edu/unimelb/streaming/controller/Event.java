package edu.unimelb.streaming.controller;

public enum Event {
  TupleCount, StartTopology, TotalCallDrop, CallDropList, DroppedCallEnable, TotalUserCount, SetCallDropThreshhold, SendCallDropEmail, MostCallerList, MostIntCallerList, MostNatCallerList, MostLocalCallerList, MostCallerEnable, MostIntCallerEnable, MostLocalCallerEnable, MostNatCallerEnable, MctCallDuration, HowManyToDisplay, MitDuration, MltDuration, MntDuration, CallDropWindowSize, MctWindowSize, MitWindowSize, MltWindowSize, MntWindowSize, MctReward, MitReward, MltReward, MntReward, StopTopology
}
