package edu.unimelb.streaming;

import java.awt.EventQueue;
import java.util.Map;

import edu.unimelb.streaming.controller.CdrTemplateController;
import edu.unimelb.streaming.topology.CdrMarketingTopology;
import edu.unimelb.streaming.util.EmailUtil;
import edu.unimelb.streaming.view.CdrMainGUI;

public class CdrMarketingTemplate {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          CdrTemplateController controller = CdrTemplateController.getInstance();
          CdrMainGUI window = new CdrMainGUI(controller);
          Map<String, Long> configuration = window.getConfiguration();
          EmailUtil emailUtil = new EmailUtil(controller);
          CdrMarketingTopology topology = new CdrMarketingTopology(controller);
          topology.setConfiguration(configuration);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

  }
}
